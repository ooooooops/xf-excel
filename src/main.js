/*
 * @Description: 
 * @Author: flyer
 * @Date: 2021-11-03 09:15:35
 * @LastEditTime: 2022-03-30 10:44:53
 * @LastEditors: flyer
 */
import Vue from 'vue'
import App from './App.vue'
import Element from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css';
import Print from 'vue-print-nb'
Vue.use(Print)
Vue.use(Element)
Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')
